# Glacial Lake Segmentation Using Geo-Foundation Model

This repository extends the Prithvi remote sensing foundation model from the paper *"Glacial Lake Mapping Using Remote Sensing Geo-Foundation Model"*. It integrates Prithvi as a U-ViT backbone for training with the TorchGeo library, enhancing glacial lake segmentation from multi-source remote sensing imagery.

## The architectures

we developed a U-ViT model based on IBM-NASA's Prithvi GFM. The model features a U-shaped encoder-decoder architecture, utilizing the Prithvi GFM as the encoder backbone, with a CNN-based decoder. We integrated an enhanced Squeeze-and-Excitation block for multi-source data fusion and input data dimension alignment, and introduced an Inverted Bottleneck block to improve local feature extraction. 
## Requirements

```
torch>=2.1.1
numpy>=1.26.2
omegaconf>=2.3.0
einops>=0.7.0
timm>=0.9.2
segmentation-models-pytorch>=0.3.3
kornia>=0.7.0
torchmetrics>=1.2.1
torchgeo @ git+https://github.com/microsoft/torchgeo@main
```

## Examples

```
python main.py --dataroot inputdata_path --dirpath cksets_save_path --seed 123 --in_channels 6 --num_classes 2 --in_bands 5 --lr 1e-3 --patience 10 --N 3 6 9 12 --epoch 100 --log_dir tensorboard_file_save_path --model_type prithvi --ckname Name_of_task --mode train --size training1 --type all
```

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.ethz.ch/dijiang/transformer.git
git branch -M main
git push -uf origin main
```

### Datasets

The datasets used in this project, including Sentinel-1, Sentinel-2, and Gaofen-3, will be released publicly soon. In the meantime, they are available upon request.
