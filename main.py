import argparse
from torchgeo.trainers import SemanticSegmentationTask
from torchgeo.datasets.utils import RGBBandsMissingError, unbind_samples
from lightning.pytorch.callbacks import ModelCheckpoint
from lightning.pytorch.loggers import TensorBoardLogger
import matplotlib.pyplot as plt
import lightning.pytorch as pl

import torch
from utils.loss_utils import WeightBCELoss, BCELoss, FocalLoss, FocalJaccardLoss
from utils.metric_utils import metrics
from dataset.S12 import S12_lakeDataModule, plot_sample
from module.model import  get_mae_unet, get_uvit
import segmentation_models_pytorch as smp
import random
import numpy as np

import warnings
from rasterio.errors import NotGeoreferencedWarning
warnings.filterwarnings("ignore", category=NotGeoreferencedWarning)

# Set random seed for reproducibility
def set_seed(seed):
    seed = int(seed) 
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

# Define segmentation task
class PrithviSegmentationTask(SemanticSegmentationTask):
    def __init__(self, in_channels, num_classes, in_bands, lr, patience, N, model_type, type):
        super().__init__(ignore_index=255)
        self.save_hyperparameters()
        self.lr = lr
        self.N = N

    def configure_losses(self):
        if self.hparams.in_bands == 3:
            self.criterion = FocalLoss().to('cuda')
            # self.criterion = BCELoss().to('cuda')
        else:
            self.criterion = FocalJaccardLoss().to('cuda')
        # self.criterion = FocalJaccardLoss().to('cuda')
            # self.criterion = FocalLoss().to('cuda')

    def configure_metrics(self):
        num_classes = self.hparams.num_classes
        ignore_index = self.hparams.ignore_index
        self.train_metrics = metrics(num_classes, ignore_index, prefix="train_")
        self.val_metrics = metrics(num_classes, ignore_index, prefix="val_")
        self.test_metrics = metrics(num_classes, ignore_index, prefix="test_")

    def configure_models(self):
        if self.hparams.model_type == 'unet':
            self.model = smp.Unet(
                encoder_name="resnet34",
                # encoder_depth=4,
                encoder_weights="imagenet",
                # decoder_channels=(512,256, 128, 64),
                in_channels=self.hparams.in_bands,
                classes=self.hparams.num_classes
                
            ).to('cuda')
            
        elif self.hparams.model_type == 'unet++':
            self.model = smp.UnetPlusPlus(
                encoder_name="resnet34",
                # encoder_depth=4,
                encoder_weights="imagenet",
                # decoder_channels=(512,256, 128, 64),
                in_channels=self.hparams.in_bands,
                classes=self.hparams.num_classes
            ).to('cuda')
            
        elif self.hparams.model_type == 'deeplabv3+':
            self.model = smp.DeepLabV3Plus(
                encoder_name="resnet34",
                encoder_weights="imagenet",
                in_channels=self.hparams.in_bands,
                classes=self.hparams.num_classes
            ).to('cuda')
            
        elif self.hparams.model_type == 'manet':
            self.model = smp.MAnet(
                encoder_name="resnet34",
                # encoder_depth=4,
                encoder_weights="imagenet",
                # decoder_channels=(512,256, 128, 64),
                in_channels=self.hparams.in_bands,
                classes=self.hparams.num_classes
            ).to('cuda')
            
        elif self.hparams.model_type == 'prithvi':
            self.model = get_uvit(
                in_channels=self.hparams.in_channels,
                num_classes=self.hparams.num_classes,
                in_bands=self.hparams.in_bands,
                img_size=224,
                N=self.hparams.N,
                type = self.hparams.type
            ).to('cuda')
                
            
        elif self.hparams.model_type == 'mae':
            self.model = get_mae_unet(
                in_channels=self.hparams.in_channels,
                num_classes=self.hparams.num_classes,
                in_bands=self.hparams.in_bands,
                img_size=224,
                N=self.hparams.N,
                type = self.hparams.type
            ).to('cuda')
            
        
        else:
            raise ValueError(f"Unsupported model type: {self.hparams.model_type}")
        self.count_parameters()
        
    def count_parameters(self):
        total_params = sum(p.numel() for p in self.model.parameters() if p.requires_grad)
        print(f"Total Trainable Parameters: {total_params}")
        if self.logger:
            self.logger.log_hyperparams({"Total Trainable Parameters": total_params})
    

    def configure_optimizers(self):
        if self.hparams.in_bands == 3:
            optimizer = torch.optim.AdamW(self.model.parameters(), lr=self.lr, weight_decay=1e-5)
            # optimizer = torch.optim.AdamW(self.model.parameters(), lr=self.lr)
        else:
            optimizer = torch.optim.AdamW(self.model.parameters(), lr=self.lr, weight_decay=1e-7)
            # optimizer = torch.optim.AdamW(self.model.parameters(), lr=self.lr)
        
        scheduler = torch.optim.lr_scheduler.OneCycleLR(
            optimizer, max_lr=self.lr, total_steps=self.trainer.estimated_stepping_batches
        )
        return [optimizer], [scheduler]

    def training_step(self, batch, batch_idx, dataloader_idx=0):
        x = batch["image"].to('cuda')
        y = batch["mask"].to('cuda')
        y_hat = self(x)
        y_hat_hard = y_hat.argmax(dim=1)
        loss = self.criterion(y_hat, y)
        self.log("train_loss", loss)
        self.train_metrics(y_hat_hard, y)
        self.log_dict({f"{k}": v for k, v in self.train_metrics.compute().items()})
        return loss

    def validation_step(self, batch, batch_idx, dataloader_idx=0):
        x = batch["image"].to('cuda')
        y = batch["mask"].to('cuda')
        y_hat = self(x)
        y_hat_hard = y_hat.argmax(dim=1)
        loss = self.criterion(y_hat, y)
        self.log("val_loss", loss)
        self.val_metrics(y_hat_hard, y)
        self.log_dict({f"{k}": v for k, v in self.val_metrics.compute().items()})

        # Plot samples
        if (
            batch_idx < 20
            and hasattr(self.trainer, "datamodule")
            and hasattr(self.trainer.datamodule, "plot")
            and self.logger
            and hasattr(self.logger, "experiment")
            and hasattr(self.logger.experiment, "add_figure")
        ):
            datamodule = self.trainer.datamodule
            batch["prediction"] = y_hat_hard.cpu()
            for key in ["image", "mask", "prediction"]:
                batch[key] = batch[key].cpu()
            sample = unbind_samples(batch)[0]

            fig = None
            try:
                fig = datamodule.plot(sample)
            except RGBBandsMissingError:
                pass

            if fig:
                summary_writer = self.logger.experiment
                summary_writer.add_figure(
                    f"cd /{batch_idx}", fig, global_step=self.global_step
                )
                plt.close()

    def test_step(self, batch, batch_idx, dataloader_idx=0):
        x = batch["image"].to('cuda')
        y = batch["mask"].to('cuda')
        y_hat = self(x)
        y_hat_hard = y_hat.argmax(dim=1)
        loss = self.criterion(y_hat, y)
        self.log("test_loss", loss)
        self.test_metrics(y_hat_hard, y)
        self.log_dict({f"{k}": v for k, v in self.test_metrics.compute().items()})

        # Plot samples
        if (
            hasattr(self.logger, "experiment")
            and hasattr(self.logger.experiment, "add_figure")
        ):
            batch = {"image": x.cpu(), "mask": y.cpu(), "prediction": y_hat_hard.cpu()}
            samples = unbind_samples(batch)
            for i, sample in enumerate(samples):
                fig = plot_sample(sample["image"], sample["mask"], sample["prediction"])

                if fig:
                    summary_writer = self.logger.experiment
                    summary_writer.add_figure(
                        f"Test/Prediction/{batch_idx}_{i}", fig, global_step=self.global_step
                    )
                    plt.close(fig)
                else:
                    print(f"No figure generated for sample {i} in batch {batch_idx}")

def main(args):
    set_seed(args.seed)  # 设置随机种子

    if args.mode == "train":
        # Initialize Model, datamodule and trainer
        module = PrithviSegmentationTask(
            in_channels=args.in_channels,
            num_classes=args.num_classes,
            in_bands=args.in_bands,
            lr=args.lr,
            patience=args.patience,
            # bd = args.bd,
            N=args.N,
            model_type=args.model_type, 
            type = args.type
        )
        Taskname = '0812'
        
        custom_directories = {"train": args.size, "val": "validation", "tst": "validation"}  # data from Sentinel12
        datamodule = S12_lakeDataModule(
            root=args.dataroot,  # data from Sentinel12
            directories=custom_directories,
            batch_size=16,
            num_workers=8,
        )
        print(f"ckname: {args.ckname}")
        
        # Initialize TensorBoard logger
        logger = TensorBoardLogger(save_dir=args.log_dir, name=Taskname,version=f'{args.ckname}')

        checkpoint_callback = ModelCheckpoint(
            monitor='val_AverageF1Score',
            dirpath=args.dirpath,
            filename=f'{args.model_type}_{args.ckname}-{{epoch:02d}}-{{val_loss:.2f}}-{{val_AverageF1Score:.2f}}',
            save_top_k=4,
            mode='max'
        )
        trainer = pl.Trainer(
            accelerator="gpu", devices=[0], logger=logger, max_epochs=args.epoch, precision="16-mixed", callbacks=[checkpoint_callback]
        )

        # 开始训练
        trainer.fit(model=module, datamodule=datamodule)

        # 确保训练结束后关闭 TensorBoard 记录器
        logger.experiment.flush()
        logger.experiment.close()

    elif args.mode == "test":
        # 进行预测
        trainer = pl.Trainer(accelerator="gpu", devices=[0], logger=TensorBoardLogger(save_dir=args.log_dir, name='TestLogs'), max_epochs=100, precision="16-mixed")
        module = PrithviSegmentationTask.load_from_checkpoint(args.checkpoint_path, in_channels=args.in_channels, num_classes=args.num_classes, lr=args.lr, patience=args.patience, N=args.N, model_type=args.model_type)
        custom_directories = {"train": args.size, "val": "validation", "tst": "validation"}  # data from Sentinel12
        datamodule = S12_lakeDataModule(
            root=args.dataroot,  # data from Sentinel12
            directories=custom_directories,
            batch_size=16,
            num_workers=8,
        )
        # 使用trainer.test进行数据预测
        trainer.test(model=module, datamodule=datamodule)

        # 确保测试结束后关闭 TensorBoard 记录器
        trainer.logger.experiment.flush()
        trainer.logger.experiment.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataroot", type=str, default='/home/dijiang/fast_1/dijiang/Sentinel1_2_Data_Fusion/', help="Root directory for the dataset")
    parser.add_argument("--dirpath", type=str, default='/data/local/fast-1/dijiang/MAE/cksetsall/modelcomp/final3',help="Directory path for saving checkpoints")
    parser.add_argument("--log_dir", type=str, default='/home/dijiang/MAE/.vscode/logs/', help="Directory for TensorBoard logs")
    parser.add_argument("--checkpoint_path", type=str, help="Path to the checkpoint for prediction")
    parser.add_argument("--in_channels", type=int, default=6, help="Number of input channels")
    parser.add_argument("--in_bands", type=int, default=5, help="Number of input bands")
    parser.add_argument("--num_classes", type=int, default=2, help="Number of output classes")
    parser.add_argument("--seed", type=float, default=123, help="Number of the seed")
    parser.add_argument("--lr", type=float, default=1e-3, help="Learning rate")
    parser.add_argument("--patience", type=int, default=10, help="Patience for early stopping")
    parser.add_argument("--N", type=int, nargs='+', default=[3,6,9,12], help="List of layers for the model")
    parser.add_argument("--epoch", type=int, default=100, help="Number of epoch")
    parser.add_argument("--model_type", type=str, default="prithvi", choices=["unet", "deeplabv3+", "manet", "prithvi", "mae","vit","unet++","noskip"], help="Type of model to use")
    parser.add_argument("--ckname", type=str, default='tst_transformer_new ', help="Name the ckpt")
    parser.add_argument("--mode", type=str, default="train", choices=["train", "test"], help="Mode to run: train or test")
    parser.add_argument("--size", type=str, default="training1", choices=["training1", "training0.8", "training0.5","training0.3"], help="Training size 1,0.8,0.5,0.3")
    parser.add_argument("--type", type=str, default="all",  help="Ablation study")

    # parser.add_argument("--bd", type=str, default=0, help="Which band delete")

    args = parser.parse_args()
    main(args)
