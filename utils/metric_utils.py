# metric_utils.py
import torch
from torchmetrics import MetricCollection
from torchmetrics.classification import Accuracy, FBetaScore, JaccardIndex, Precision, Recall
from torchmetrics.wrappers import ClasswiseWrapper

def metrics(num_classes, ignore_index, prefix):
    metrics = MetricCollection(
        {
            "OverallAccuracy": Accuracy(
                task="multiclass",
                num_classes=num_classes,
                average="micro",
                multidim_average="global",
            ),
            "OverallF1Score": FBetaScore(
                task="multiclass",
                num_classes=num_classes,
                beta=1.0,
                average="micro",
                multidim_average="global",
            ),
            "OverallIoU": JaccardIndex(
                task="multiclass",
                num_classes=num_classes,
                ignore_index=ignore_index,
                average="micro",
            ),
            "AverageAccuracy": Accuracy(
                task="multiclass",
                num_classes=num_classes,
                average="macro",
                multidim_average="global",
            ),
            "AveragePrecision": Precision(
                task="multiclass",
                num_classes=num_classes,
                average="macro",
                multidim_average="global",
            ),
            "AverageRecall": Recall(
                task="multiclass",
                num_classes=num_classes,
                average="macro",
                multidim_average="global",
            ),
            "AverageF1Score": FBetaScore(
                task="multiclass",
                num_classes=num_classes,
                beta=1.0,
                average="macro",
                multidim_average="global",
            ),
            "AverageIoU": JaccardIndex(
                task="multiclass",
                num_classes=num_classes,
                ignore_index=ignore_index,
                average="macro",
            ),
            "Accuracy": ClasswiseWrapper(
                Accuracy(
                    task="multiclass",
                    num_classes=num_classes,
                    average="none",
                    multidim_average="global",
                ),
            ),
            "F1Score": ClasswiseWrapper(
                FBetaScore(
                    task="multiclass",
                    num_classes=num_classes,
                    beta=1.0,
                    average="none",
                    multidim_average="global",
                ),
            ),
        },
        prefix=prefix,
    ).to('cuda')
    return metrics
