import torch
import torch.nn as nn
import segmentation_models_pytorch as smp

# Define Weight BCE Loss
class WeightBCELoss(nn.Module):
    def __init__(self):
        super().__init__()
        weights = [0.001, 0.999]
        class_weights = torch.FloatTensor(weights)
        self.ce_loss = nn.CrossEntropyLoss(weight=class_weights)

    def forward(self, preds: torch.Tensor, targets: torch.Tensor) -> torch.Tensor:
        return self.ce_loss(preds, targets)
# Define Weight BCE Loss
class BCELoss(nn.Module):
    def __init__(self):
        super().__init__()
        weights = [0.001, 0.999]
        self.ce_loss = nn.CrossEntropyLoss()
    def forward(self, preds: torch.Tensor, targets: torch.Tensor) -> torch.Tensor:
        return self.ce_loss(preds, targets)
      
# Define Focal Loss
class FocalLoss(nn.Module):
    def __init__(self):
        super().__init__()
        self.focal_loss = smp.losses.FocalLoss(mode="multiclass", gamma=2, alpha=0.8) #2 0.8 for Gaofen-3 ///////   2  0.06 for Sentinel-1&2

    def forward(self, preds: torch.Tensor, targets: torch.Tensor) -> torch.Tensor:
        return self.focal_loss(preds, targets)
    
# Define Focal Jaccard Loss
class FocalJaccardLoss(nn.Module):
    def __init__(self):
        super().__init__()
        self.focal_loss = smp.losses.FocalLoss(mode="multiclass", gamma=2, alpha=0.06)
        self.jaccard_loss = smp.losses.JaccardLoss(mode="multiclass")

    def forward(self, preds: torch.Tensor, targets: torch.Tensor) -> torch.Tensor:
        focal_loss_value = self.focal_loss(preds, targets)
        jaccard_loss_value = self.jaccard_loss(preds, targets)
        return (0.5*focal_loss_value + 0.5*jaccard_loss_value)
