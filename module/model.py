# models.py
import torch
from module.UViT import UViT, MAEUnet

def get_mae_unet(in_channels,num_classes,in_bands,N,type,img_size=224):
    return MAEUnet(
        in_channels=in_channels,
        out_channels=num_classes,
        in_bands = in_bands,
        img_size=img_size,
        n=N,
        freeze_encoder=False,
        type = type,
    ).to('cuda')


def get_uvit(in_channels,num_classes,in_bands, N,type,img_size=224):
    return UViT(
        in_channels=in_channels,
        out_channels=num_classes,
        in_bands = in_bands,
        img_size=img_size,
        n=N,
        freeze_encoder=False,
        type = type,
    ).to('cuda')