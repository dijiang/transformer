import glob
import os
from typing import Any, Callable, Optional

import kornia.augmentation as K
import matplotlib.pyplot as plt
import numpy as np
import rasterio
import torch
from matplotlib.figure import Figure
from torch.utils.data import Dataset
from torchgeo.datamodules import NonGeoDataModule
from torchgeo.datasets.utils import percentile_normalization
from torchgeo.transforms import AugmentationSequential
from transformers import AutoImageProcessor
from sklearn.decomposition import PCA

def plot_sample(image, mask, prediction):
    fig, axes = plt.subplots(1, 3, figsize=(15, 5))
    
    # Assuming image is in CHW format, convert it to HWC format
    image = image[:3].numpy()
    image = np.rollaxis(image, 0, 3)
    image = image[..., ::-1]
    image = percentile_normalization(image, axis=(0, 1))
    
    mask = mask.numpy()
    prediction = prediction.numpy()

    # Normalize image for better visualization
    image = (image - image.min()) / (image.max() - image.min())

    axes[0].imshow(image)
    axes[0].set_title("Image")
    axes[1].imshow(mask, cmap='gray')
    axes[1].set_title("Mask")
    axes[2].imshow(prediction, cmap='gray')
    axes[2].set_title("Prediction")

    for ax in axes:
        ax.axis("off")

    return fig

def PCA_combine(input_image: torch.Tensor, outband: int) -> torch.Tensor:
    num_pixels = input_image.shape[1] * input_image.shape[2]
    num_bands = input_image.shape[0]
    input_data = input_image.view(num_bands, -1).numpy().T
    pca = PCA(n_components=outband)
    principal_components = pca.fit_transform(input_data)
    new_data= principal_components.T[:outband]
    new_data = torch.tensor(new_data).view(outband, input_image.shape[1], input_image.shape[2])
    return new_data

class S12_lake(Dataset):
    def __init__(self, root: str, split: str = "train", directories: dict = None, transforms: Optional[Callable] = None):
        assert split in ["train", "val","tst"]
        self.transforms = transforms
        if directories is None:
            directories = {"train": "training1", "val": "validation","tst":"test"}
        self.directories = directories
        directory = os.path.join(root, self.directories[split])
        imgs_dir = os.path.join(directory, 'imgs')
        label_dir = os.path.join(directory, 'labels')
        self.images = sorted(glob.glob(f"{imgs_dir}/*.tif"))
        self.masks = sorted(glob.glob(f"{label_dir}/*.tif"))
    
    def __len__(self) -> int:
        return len(self.images)

    def __getitem__(self, index: int) -> dict[str, torch.Tensor]:
        image = self.load_image(index)
        mask = self.load_mask(index)
        sample = {"image": image, "mask": mask}

        if self.transforms is not None:
            sample = self.transforms(sample)

        return sample
    
    def load_image(self, index: int) -> torch.Tensor:
        path = self.images[index]
        with rasterio.open(path) as dataset:
            image = dataset.read().astype(np.float32)
            image_new = np.zeros_like(image)
            for i in range(image.shape[0]):
                band = image[i, :, :]
                mean = np.mean(band)
                std = np.std(band)
                if std == 0:
                    std = 1e-6
                image_new[i, :, :] = (band - mean) / std
                new_band1 = image_new[0, :, :]
        
        # new_band2 = image_new[0, :, :]
        # new_band2 = image_new[1, :, :]
        # new_band1 = np.zeros_like(image_new[0, :, :])
        # new_band2 = np.zeros_like(image_new[0, :, :])
        
        # # 叠加新的波段
        # new_bands = np.stack((new_band1, new_band2), axis=0)
        # image_new = np.concatenate((image_new, new_bands), axis=0)
        
        image_new = torch.from_numpy(image_new.astype(np.float32))
        ## compare  different bands
        # selected_bands = [1, 2, 3, 4] 
        # image_new = image_new[selected_bands , :, :]
        return image_new

    def load_mask(self, index: int) -> torch.Tensor:
        path = self.masks[index]
        with rasterio.open(path) as dataset:
            mask = dataset.read(indexes=1).astype(np.uint8)
            mask = torch.from_numpy(mask).to(torch.long)
            mask = torch.clip(mask, 0, 1)
            return mask

    def plot(self, sample: dict[str, torch.Tensor], show_titles: bool = True, suptitle: Optional[str] = None) -> Figure:
        image = sample["image"][:3].numpy()
        image = np.rollaxis(image, 0, 3)
        image = image[..., ::-1]
        image = percentile_normalization(image, axis=(0, 1))

        ncols = 1
        show_mask = "mask" in sample
        show_predictions = "prediction" in sample

        if show_mask:
            mask = sample["mask"].numpy()
            ncols += 1

        if show_predictions:
            prediction = sample["prediction"].numpy()
            ncols += 1

        fig, axs = plt.subplots(ncols=ncols, figsize=(ncols * 8, 8))
        if not isinstance(axs, np.ndarray):
            axs = [axs]
        axs[0].imshow(image)
        axs[0].axis("off")
        if show_titles:
            axs[0].set_title("Image")

        if show_mask:
            axs[1].imshow(mask, interpolation="none")
            axs[1].axis("off")
            if show_titles:
                axs[1].set_title("Label")

        if show_predictions:
            axs[2].imshow(prediction, interpolation="none")
            axs[2].axis("off")
            if show_titles:
                axs[2].set_title("Prediction")

        if suptitle is not None:
            plt.suptitle(suptitle)
        return fig

class S12_lakeDataModule(NonGeoDataModule):
    mean = torch.zeros(6)
    std = torch.ones(6)

    image_size = (224, 224)

    def __init__(self, batch_size: int = 8, num_workers: int = 0, directories: dict = None, **kwargs: Any) -> None:
        super().__init__(S12_lake, batch_size=batch_size, num_workers=num_workers, **kwargs)
        self.directories = directories
        self.train_aug = AugmentationSequential(
            K.Resize(size=self.image_size),
            data_keys=["image", "mask"],
        )
        self.val_aug = AugmentationSequential(
            K.Resize(size=self.image_size),
            data_keys=["image", "mask"],
        )
        self.test_aug = AugmentationSequential(
            K.Resize(size=self.image_size),
            data_keys=["image", "mask"],
        )

    def setup(self, stage: str) -> None:
        if stage in ["fit", "validate"]:
            self.train_dataset = S12_lake(split="train", directories=self.directories, **self.kwargs)
            self.val_dataset = S12_lake(split="val", directories=self.directories, **self.kwargs)
        if stage in ["test"]:
            self.test_dataset = S12_lake(split="tst", directories=self.directories, **self.kwargs)


# ############# Split traning and validation in the same folder ###########################
# import glob
# import os
# from typing import Any, Callable, Optional

# import matplotlib.pyplot as plt
# import numpy as np
# import rasterio
# import torch
# import kornia.augmentation as K
# from matplotlib.figure import Figure
# from torch.utils.data import Dataset
# from torchgeo.datamodules import NonGeoDataModule
# from torchgeo.datasets.utils import percentile_normalization
# from torch.utils.data import DataLoader, random_split
# import torchvision.transforms as transforms
# from torchgeo.transforms import AugmentationSequential
# from transformers import AutoImageProcessor

# class S12_lake(Dataset):
#     def __init__(self, root: str,transforms=None):
#         imgs = sorted(glob.glob(os.path.join(root, 'imgs/*.tif')))  # 子路径
#         imgs_rs = sorted(glob.glob(os.path.join(root, 'rote_scale/imgs/*.tif')))
#         imgs_gussian = sorted(glob.glob(os.path.join(root, 'Gussian/imgs/*.tif')))
#         self.images = imgs #+imgs_rs #+imgs_gussian
#         self.transforms = transforms
    
#     def __len__(self) -> int:
#         return len(self.images)

#     def __getitem__(self, index: int) -> dict[str, torch.Tensor]:
#         image = self.load_image(index)
#         mask = self.load_mask(index)
#         sample = {"image": image, "mask": mask}

#         if self.transforms is not None:
#             sample = self.transforms(sample)

#         return sample

#     def load_image(self, index: int) -> torch.Tensor:
#         path = self.images[index]
#         with rasterio.open(path) as dataset:
#             image = dataset.read().astype(np.float32)
#             if image.shape[0]<=3:
#                 image=image[0:3, :, :]
#                 image_new = np.zeros((3, image.shape[1], image.shape[2]))
#                 for i in range(3):  # 循环前三个波段
#                     band = image[i, :, :]
#                     mean = np.mean(band)
#                     std = np.std(band)
#                     # Z-score 标准化
#                     image_new[i, :, :] = (band - mean) / std
#                     # 将标准化后的波段复制到后三个波段
#                 # image_new = np.concatenate((image_new,image_new),axis=0)      
#             else:
#                 image_new = np.zeros_like(image)
#                 for i in range(image.shape[0]):
#                     band = image[i, :, :]
#                     mean = np.mean(band)
#                     std = np.std(band)
#                     if std == 0:
#                         # If std is zero, leave the band unchanged
#                         std=1e-6
#                     image_new[i, :, :] = (band - mean) / std
#                 sub = image_new[4,:,:]
#                 image_new = np.concatenate((image_new,np.expand_dims(sub, axis=0)),axis=0)                      
#             image_new = torch.from_numpy(image_new.astype(np.float32))
#         return image_new


#     def load_mask(self, index: int) -> torch.Tensor:
#         path = self.images[index]
#         path = path.replace('imgs', 'labels')
#         with rasterio.open(path) as dataset:
#             mask = dataset.read(indexes=1).astype(np.uint8)
#             mask = torch.from_numpy(mask).to(torch.long)
#             mask = torch.clip(mask, 0, 1)
#             return mask

#     def plot(
#         self,
#         sample: dict[str, torch.Tensor],
#         show_titles: bool = True,
#         suptitle: Optional[str] = None,
#     ) -> Figure:
#         image = sample["image"][:3].numpy()  # get BGR channels & convert to numpy
#         image = np.rollaxis(image, 0, 3)  # CHW -> HWC
#         image = image[..., ::-1]  # bgr to rgb
#         image = percentile_normalization(
#             image, axis=(0, 1)
#         )  # normalize for visualizing

#         ncols = 1
#         show_mask = "mask" in sample
#         show_predictions = "prediction" in sample

#         if show_mask:
#             mask = sample["mask"].numpy()
#             ncols += 1

#         if show_predictions:
#             prediction = sample["prediction"].numpy()
#             ncols += 1

#         fig, axs = plt.subplots(ncols=ncols, figsize=(ncols * 8, 8))
#         if not isinstance(axs, np.ndarray):
#             axs = [axs]
#         axs[0].imshow(image)
#         axs[0].axis("off")
#         if show_titles:
#             axs[0].set_title("Image")

#         if show_mask:
#             axs[1].imshow(mask, interpolation="none")
#             axs[1].axis("off")
#             if show_titles:
#                 axs[1].set_title("Label")

#         if show_predictions:
#             axs[2].imshow(prediction, interpolation="none")
#             axs[2].axis("off")
#             if show_titles:
#                 axs[2].set_title("Prediction")

#         if suptitle is not None:
#             plt.suptitle(suptitle)
#         return fig

# class S12_lakeDataModule(NonGeoDataModule):
#     image_size = (224, 224)
    
#     def __init__(
#         self,
#         batch_size: int = 8,
#         num_workers: int = 0,
#         val_percent: float=0.1,
#         **kwargs: Any,
#     ) -> None:
#         super().__init__(
#             S12_lake, batch_size=batch_size, num_workers=num_workers, **kwargs
#         )
        

#         self.val_percent = val_percent
#         self.train_aug = AugmentationSequential(
#             K.Resize(size=self.image_size),
#             # K.RandomHorizontalFlip(p=0.5),
#             # K.RandomVerticalFlip(p=0.5),
#             data_keys=["image", "mask"],
#         )
#         self.val_aug = AugmentationSequential(
#             K.Resize(size=self.image_size),
#             data_keys=["image", "mask"],
#         )
#         self.test_aug = AugmentationSequential(
#             K.Resize(size=self.image_size),
#             data_keys=["image", "mask"],
#         )
#     def setup(self, stage: str) -> None:
#         if stage in ["fit", "validate"]:
#             dataset = S12_lake(**self.kwargs)
#             n_val = int(len(dataset) * self.val_percent)
#             n_train = len(dataset) - n_val
#             train_dataset, val_dataset = random_split(dataset, [n_train, n_val])
#             self.train_dataset = train_dataset.dataset
#             self.val_dataset = val_dataset.dataset


